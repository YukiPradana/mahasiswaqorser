package com.example.MahasiswaQorser.Entity;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "jurusan")
public class Jurusan {
    @Id
    @GeneratedValue
    private int id;
    @Column(
            nullable = false,
            unique = true
    )
    private String namaJurusan;

    @OneToMany(mappedBy = "jurusanMahasiswa")
    private List<Mahasiswa> mahasiswas;

    @OneToMany(mappedBy = "jurusanMatkul")
    private List<MataKuliah> mataKuliahs;
}
