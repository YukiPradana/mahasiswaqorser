package com.example.MahasiswaQorser.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "rencana_studi")
public class RencanaStudi {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private Mahasiswa fsrMahasiswa;

    @ManyToOne
    private MataKuliah fsrMataKuliah;
}
