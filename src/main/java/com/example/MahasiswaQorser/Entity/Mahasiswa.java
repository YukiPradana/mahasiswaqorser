package com.example.MahasiswaQorser.Entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "mahasiswa")
public class Mahasiswa {

    @Id
    @GeneratedValue
    @Column(updatable = false)
    private int id;


    private String namaLengkap;

    @Column(
            nullable = false,
            unique = true
    )
    private int nim;

    @ManyToOne
    private Jurusan jurusanMahasiswa;

    @OneToMany(mappedBy = "fsrMahasiswa")
    private List<RencanaStudi> mahasiswaFSR;
}
