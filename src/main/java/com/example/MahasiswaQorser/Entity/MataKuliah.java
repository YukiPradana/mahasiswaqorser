package com.example.MahasiswaQorser.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "mata_kuliah")
public class MataKuliah {
    @Id
    @GeneratedValue
    private int id;

    private String namaMatkul;

    private Date tanggal;

    private Time jam;
    private String tempat;

    @ManyToOne
    private Jurusan jurusanMatkul;

    @OneToMany(mappedBy = "fsrMataKuliah")
    private List<RencanaStudi> mataKuliahFsr;

}
