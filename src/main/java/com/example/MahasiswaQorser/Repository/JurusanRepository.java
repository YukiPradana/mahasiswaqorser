package com.example.MahasiswaQorser.Repository;

import com.example.MahasiswaQorser.Entity.Jurusan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JurusanRepository extends JpaRepository<Jurusan,Integer> {
}
