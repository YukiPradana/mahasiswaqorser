package com.example.MahasiswaQorser.Repository;

import com.example.MahasiswaQorser.Entity.RencanaStudi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RencanaStudiRepository extends JpaRepository<RencanaStudi,Integer> {
    List<RencanaStudi> findByFsrMahasiswa(int id);
}
