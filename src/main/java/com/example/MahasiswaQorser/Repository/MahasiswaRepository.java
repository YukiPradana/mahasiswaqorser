package com.example.MahasiswaQorser.Repository;

import com.example.MahasiswaQorser.Entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa,Integer> {
    Mahasiswa findByNim(int nim);
}
