package com.example.MahasiswaQorser.Repository;

import com.example.MahasiswaQorser.Entity.Jurusan;
import com.example.MahasiswaQorser.Entity.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MataKuliahRepository extends JpaRepository<MataKuliah,Integer> {
    List<MataKuliah>findByJurusanMatkul(Jurusan jurusan);
}
