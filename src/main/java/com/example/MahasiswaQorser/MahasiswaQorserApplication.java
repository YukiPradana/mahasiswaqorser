package com.example.MahasiswaQorser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MahasiswaQorserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MahasiswaQorserApplication.class, args);
	}

}
