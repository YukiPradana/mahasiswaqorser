package com.example.MahasiswaQorser.Service;

import com.example.MahasiswaQorser.Entity.Mahasiswa;
import com.example.MahasiswaQorser.Repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaService {
    @Autowired
    private MahasiswaRepository repository;

    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa){
        return repository.save(mahasiswa);
    }

    public List<Mahasiswa> createAllMahasiswa (List<Mahasiswa> mahasiswas){
        return repository.saveAll(mahasiswas);
    }

    public List<Mahasiswa> getMahasiswa(){
        return repository.findAll();
    }

    public Mahasiswa getMahasiswaById(int id){
        return repository.findById(id).orElse(null);
    }

    public Mahasiswa getMahasiswaByNIM(int nim){
        return repository.findByNim(nim);
    }

    public Mahasiswa updateMahasiswa(Mahasiswa mahasiswa){
        Mahasiswa existingMahasiswa=repository.findById(mahasiswa.getId()).orElse(null);
        existingMahasiswa.setJurusanMahasiswa(mahasiswa.getJurusanMahasiswa());
        existingMahasiswa.setNamaLengkap(mahasiswa.getNamaLengkap());
        existingMahasiswa.setNim(mahasiswa.getNim());
        existingMahasiswa.setMahasiswaFSR(mahasiswa.getMahasiswaFSR());
        return repository.save(existingMahasiswa);
    }

    public String deleteByID(int id){
        repository.deleteById(id);
        return id+" deleted";
    }
}
