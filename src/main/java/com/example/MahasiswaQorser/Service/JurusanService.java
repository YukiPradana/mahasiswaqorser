package com.example.MahasiswaQorser.Service;

import com.example.MahasiswaQorser.Entity.Jurusan;
import com.example.MahasiswaQorser.Repository.JurusanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JurusanService {
    @Autowired
    private JurusanRepository repository;

    public Jurusan createJurusan(Jurusan jurusan){
        return repository.save(jurusan);
    }

    public List<Jurusan> createAllJurusan(List<Jurusan> jurusanList){
        return repository.saveAll(jurusanList);
    }

    public List<Jurusan> getJurusan(){
        return repository.findAll();
    }

    public Jurusan getJurusanByID(int id){
        return repository.findById(id).orElse(null);
    }

    public Jurusan updateJurusan(Jurusan jurusan){
        Jurusan existingJurusan=repository.findById(jurusan.getId()).orElse(null);
        existingJurusan.setNamaJurusan(jurusan.getNamaJurusan());
        return repository.save(existingJurusan);
    }

    public String deleteJurusanById(int id){
        repository.deleteById(id);
        return id+" deleted";
    }
}
