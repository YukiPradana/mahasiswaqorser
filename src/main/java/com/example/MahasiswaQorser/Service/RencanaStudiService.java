package com.example.MahasiswaQorser.Service;

import com.example.MahasiswaQorser.Entity.RencanaStudi;
import com.example.MahasiswaQorser.Repository.RencanaStudiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RencanaStudiService {
    @Autowired
    private RencanaStudiRepository repository;

    public RencanaStudi createRencanaStudi(RencanaStudi rencanaStudi){
        return repository.save(rencanaStudi);
    }

    public List<RencanaStudi> createAllRencanaStudi(List<RencanaStudi> rencanaStudis){
        return repository.saveAll(rencanaStudis);
    }

    public List<RencanaStudi> getRencanaStudi(){
        return repository.findAll();
    }

    public RencanaStudi getRencanaStudiById(int id){
        return repository.findById(id).orElse(null);
    }

    public List<RencanaStudi> getRencanaByMahasiswa(int id){
        return repository.findByFsrMahasiswa(id);
    }
    public RencanaStudi updateRencanaStudi(RencanaStudi rencanaStudi){
        RencanaStudi existingRencana=repository.findById(rencanaStudi.getId()).orElse(null);
        existingRencana.setFsrMahasiswa(rencanaStudi.getFsrMahasiswa());
        existingRencana.setFsrMataKuliah(rencanaStudi.getFsrMataKuliah());
        return repository.save(existingRencana);
    }

    public String deleteRencanaStudiById (int id){
        repository.deleteById(id);
        return id+" deleted";
    }

}
