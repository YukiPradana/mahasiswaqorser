package com.example.MahasiswaQorser.Service;

import com.example.MahasiswaQorser.Entity.Jurusan;
import com.example.MahasiswaQorser.Entity.MataKuliah;
import com.example.MahasiswaQorser.Repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MataKuliahService {
    @Autowired
    MataKuliahRepository repository;

    public MataKuliah createMataKuliah(MataKuliah mataKuliah){
        return repository.save(mataKuliah);
    }

    public List<MataKuliah> createAllMataKuliah(List<MataKuliah> mataKuliahs){
        return repository.saveAll(mataKuliahs);
    }

    public List<MataKuliah> getMataKuliah(){
        return repository.findAll();
    }

    public MataKuliah getMataKuliahById(int id){
        return repository.findById(id).orElse(null);
    }

    public List<MataKuliah>getMataKuliahByJurusan(Jurusan jurusan){
        return repository.findByJurusanMatkul(jurusan);
    }

    public MataKuliah updateMataKuliah(MataKuliah mataKuliah){
        MataKuliah existingMatkul=repository.findById(mataKuliah.getId()).orElse(null);
        existingMatkul.setNamaMatkul(mataKuliah.getNamaMatkul());
        existingMatkul.setTanggal(mataKuliah.getTanggal());
        existingMatkul.setJam(mataKuliah.getJam());
        existingMatkul.setJurusanMatkul(mataKuliah.getJurusanMatkul());
        existingMatkul.setTempat(mataKuliah.getTempat());
        existingMatkul.setMataKuliahFsr(mataKuliah.getMataKuliahFsr());
        return repository.save(existingMatkul);
    }

    public String deleteMataKuliah(int id){
        repository.deleteById(id);
        return id+" deleted";
    }
}
