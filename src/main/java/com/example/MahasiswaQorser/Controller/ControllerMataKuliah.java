package com.example.MahasiswaQorser.Controller;

import com.example.MahasiswaQorser.Entity.Jurusan;
import com.example.MahasiswaQorser.Entity.MataKuliah;
import com.example.MahasiswaQorser.Service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("mata-kuliah")
public class ControllerMataKuliah {

    @Autowired
    private MataKuliahService service;

    @PostMapping("/create")
    public MataKuliah createMataKuliah(@RequestBody MataKuliah mataKuliah){
        return service.createMataKuliah(mataKuliah);
    }

    @PostMapping("/create-all")
    public List<MataKuliah> createAllMataKuliah (@RequestBody List<MataKuliah> mataKuliahs){
        return service.createAllMataKuliah(mataKuliahs);
    }

    @GetMapping("/get")
    public List<MataKuliah> getMataKuliah(){
        return service.getMataKuliah();
    }

    @GetMapping("/get-by-id/{id}")
    public MataKuliah getMataKuliahById(@PathVariable int id){
        return service.getMataKuliahById(id);
    }

    @GetMapping("/get-by-jurusan")
    public List<MataKuliah> getMataKuliahByJurusan(@RequestBody Jurusan jurusan){
        return service.getMataKuliahByJurusan(jurusan);
    }

    @PutMapping("/update")
    public MataKuliah updateMataKuliah(@RequestBody MataKuliah mataKuliah){
        return service.updateMataKuliah(mataKuliah);
    }

    @DeleteMapping("/delete-by-id/{id}")
    public String deleteMataKuliahById (@PathVariable int id){
        return service.deleteMataKuliah(id);
    }
}
