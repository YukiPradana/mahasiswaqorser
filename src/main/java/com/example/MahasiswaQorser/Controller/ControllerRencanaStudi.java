package com.example.MahasiswaQorser.Controller;

import com.example.MahasiswaQorser.Entity.RencanaStudi;
import com.example.MahasiswaQorser.Service.RencanaStudiService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("rencana-studi")
public class ControllerRencanaStudi {

    private RencanaStudiService service;

    @PostMapping("/create")
    public RencanaStudi createRencanaStudi(@RequestBody RencanaStudi rencanaStudi){
        return service.createRencanaStudi(rencanaStudi);
    }

    @PostMapping("/create-all")
    public List<RencanaStudi> createAllRencanaStudi (@RequestBody List<RencanaStudi> rencanaStudis){
        return service.createAllRencanaStudi(rencanaStudis);
    }

    @GetMapping("/get")
    public List<RencanaStudi> getRencanaStudi (){
        return service.getRencanaStudi();
    }

    @GetMapping("/get-by-id/{id}")
    public RencanaStudi getRencanaStudById(@PathVariable int id){
        return service.getRencanaStudiById(id);
    }

    @GetMapping("/get-by-mahasiswa/{mahasiswaid}")
    public List<RencanaStudi> getRencanaStudiMahasiswa(@PathVariable int id){
        return service.getRencanaByMahasiswa(id);
    }

    @PutMapping("/update")
    public RencanaStudi updateRencanaStudi(@RequestBody RencanaStudi rencanaStudi){
        return service.updateRencanaStudi(rencanaStudi);
    }

    @DeleteMapping("/delete-by-id/{id}")
    public String deleteRencanaStudiBId(@PathVariable int id){
        return service.deleteRencanaStudiById(id);
    }
}
