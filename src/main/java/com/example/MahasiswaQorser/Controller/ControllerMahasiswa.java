package com.example.MahasiswaQorser.Controller;

import com.example.MahasiswaQorser.Entity.Mahasiswa;
import com.example.MahasiswaQorser.Service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("mahasiswa")
public class ControllerMahasiswa {
    @Autowired
    private MahasiswaService service;

    @PostMapping("/create")
    public Mahasiswa createMahasiswa(@RequestBody Mahasiswa mahasiswa){
        return service.createMahasiswa(mahasiswa);
    }

    @PostMapping("/create-all")
    public List<Mahasiswa> createAllMahasiswa (@RequestBody List<Mahasiswa> mahasiswas){
        return service.createAllMahasiswa(mahasiswas);
    }

    @GetMapping("/get")
    public List<Mahasiswa> getMahasiswa(){
        return service.getMahasiswa();
    }

    @GetMapping("/get-by-id/{id}")
    public Mahasiswa getMahasiswaById(@PathVariable int id){
        return service.getMahasiswaById(id);
    }

    @GetMapping("/get-by-nim/{nim}")
    public Mahasiswa getMahasiswaByNim(@PathVariable int nim){
        return service.getMahasiswaByNIM(nim);
    }

    @PutMapping("/update")
    public Mahasiswa updateMahasiswa(@RequestBody Mahasiswa mahasiswa){
        return service.updateMahasiswa(mahasiswa);
    }

    @DeleteMapping("/delete-by-id/{id]")
    public String deleteMahasiswaById(@PathVariable int id){
        return service.deleteByID(id);
    }
}
