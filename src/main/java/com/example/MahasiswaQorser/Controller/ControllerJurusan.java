package com.example.MahasiswaQorser.Controller;


import com.example.MahasiswaQorser.Entity.Jurusan;
import com.example.MahasiswaQorser.Service.JurusanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("jurusan")
public class ControllerJurusan {
    @Autowired
    private JurusanService service;

    @PostMapping("/add-jurusan")
    public Jurusan addJurusan(@RequestBody Jurusan jurusan){
        return service.createJurusan(jurusan);
    }

    @PostMapping("/add-all-jurusan")
    public List<Jurusan> addAllJurusan(@RequestBody List<Jurusan> jurusans){
        return service.createAllJurusan(jurusans);
    }

    @GetMapping("/get-jurusan")
    public List<Jurusan> getJurusan(){
        return service.getJurusan();
    }

    @GetMapping("/get-jurusan-by-id/{id}")
    public Jurusan getJurusanById(@PathVariable int id){
        return service.getJurusanByID(id);
    }

    @PutMapping("/put-jurusan")
    public Jurusan putJurusan(@RequestBody Jurusan jurusan){
        return service.updateJurusan(jurusan);
    }

    @DeleteMapping("/delete-jurusan/{id}")
    public String deleteJurusan(@PathVariable int id){
        return service.deleteJurusanById(id);
    }

}

